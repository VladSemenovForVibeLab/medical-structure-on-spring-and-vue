# Описание проекта

Данное приложение представляет собой систему для медицинской организации, созданную с использованием следующих технологий:

- [Spring Boot](https://spring.io/projects/spring-boot)
- [Spring Web](https://spring.io/guides/gs/spring-boot/)
- [Spring Data JPA](https://spring.io/projects/spring-data-jpa)
- [Hibernate](https://hibernate.org/)
- [Docker](https://www.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/)
- [Swagger](https://swagger.io/)

## Инструкции по установке и запуску

### 1. Установка

Прежде всего, убедитесь, что на вашем компьютере установлены следующие программы:

- Java JDK - [Инструкции по установке](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)
- Docker - [Инструкции по установке](https://www.docker.com/get-started)

### 2. Клонирование репозитория

```shell
$ git clone https://gitlab.com/VladSemenovForVibeLab/medical-structure-on-spring-and-vue.git
$ cd backend
```

### 3. Сборка и запуск приложения

```shell
$ ./mvnw clean package -DskipTests
$ docker-compose up --build
```

### 4. Доступ к приложению

После успешного запуска, приложение будет доступно по следующему URL:

```shell
http://localhost:8070
```

## API документация

API документация доступна по следующему URL:

```shell
http://localhost:8070/swagger-ui.html
```

В данной документации описаны все доступные эндпоинты, параметры запросов и примеры ответов.

## База данных

Приложение использует СУБД PostgreSQL. Данные базы данных хранятся внутри Docker контейнера.


## Авторы

- Ваше имя - https://gitlab.com/VladSemenovForVibeLab