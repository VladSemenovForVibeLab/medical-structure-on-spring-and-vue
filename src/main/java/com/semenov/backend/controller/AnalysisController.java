package com.semenov.backend.controller;

import com.semenov.backend.model.Analysis;
import com.semenov.backend.service.AnalysisService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/v1/analysis")
@RequiredArgsConstructor
public class AnalysisController {
    private final AnalysisService analysisService;
    @PostMapping("/add")
    public ResponseEntity<Analysis> createAnalysis(@RequestBody Analysis analysis) {
        Analysis createdAnalysis = analysisService.createAnalysis(analysis);
        return ResponseEntity.ok(createdAnalysis);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Analysis> getAnalysisById(@PathVariable("id") Long id) {
        Analysis analysis = analysisService.getAnalysisById(id);
        return ResponseEntity.ok(analysis);
    }
    @PutMapping
    public ResponseEntity<Analysis> updateAnalysis(@RequestBody Analysis analysis) {
        Analysis updatedAnalysis = analysisService.updateAnalysis(analysis);
        return ResponseEntity.ok(updatedAnalysis);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteAnalysis(@PathVariable("id") Long id) {
        analysisService.deleteAnalysis(id);
        return ResponseEntity.ok().build();
    }
    @GetMapping("/patient/{patientId}")
    public ResponseEntity<List<Analysis>> getAnalysesByPatientId(@PathVariable("patientId") Long patientId) {
        List<Analysis> analyses = analysisService.getAnalysesByPatientId(patientId);
        return ResponseEntity.ok(analyses);
    }
    @GetMapping("/date-range")
    public ResponseEntity<List<Analysis>> getAnalysesByDateRange(
            @RequestParam("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
            @RequestParam("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate) {
        List<Analysis> analyses = analysisService.getAnalysesByDateRange(startDate, endDate);
        return ResponseEntity.ok(analyses);
    }
}
