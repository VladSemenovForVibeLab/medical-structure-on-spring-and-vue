package com.semenov.backend.controller;

import com.semenov.backend.model.Patient;
import com.semenov.backend.model.response.MessageResponse;
import com.semenov.backend.model.response.PatientResponseMessage;
import com.semenov.backend.service.PatientService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:5175")
public class ClientController {

    private final PatientService patientService;

    @PostMapping("/add")
    public ResponseEntity<MessageResponse> addPatient(@RequestBody Patient patient){
        patientService.addPatient(patient);
        return ResponseEntity.ok(new MessageResponse("Пользователь успешно добавлен!",HttpStatus.OK));
    }
    @GetMapping("/patient/{id}")
    public ResponseEntity<PatientResponseMessage> getPatientById(@PathVariable("id") Long id){
        return ResponseEntity.ok(patientService.getPatient(id));
    }
    @GetMapping("/patients")
    public ResponseEntity<List<Patient>> getPatients(){
        return ResponseEntity.ok(patientService.getPatients());
    }
    @PutMapping("/patient/{id}")
    public ResponseEntity<PatientResponseMessage> updatePatient(@RequestBody Patient patient,
                                                 @PathVariable("id") Long id){
        return ResponseEntity.ok(patientService.updatePatient(id,patient));
    }

    @DeleteMapping("/patient/{id}")
    public ResponseEntity<MessageResponse> deletePatient(@PathVariable("id") Long id){
        patientService.deletePatient(id);
        return ResponseEntity.ok(new MessageResponse("Пациент успешно удален!",HttpStatus.OK));
    }

    @GetMapping("/patients/gender/{gender}")
    public ResponseEntity<List<Patient>> getPatientsByGender(@PathVariable("gender") String gender){
        return ResponseEntity.ok(patientService.searchPatientByGender(gender));
    }

    @GetMapping("/patients/name/{name}")
    public ResponseEntity<List<Patient>> getPatientsByName(@PathVariable("name") String name){
        return ResponseEntity.ok(patientService.searchPatientByName(name));
    }
}
