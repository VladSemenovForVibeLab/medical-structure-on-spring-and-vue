package com.semenov.backend.controller;

import com.semenov.backend.model.MedicalRecord;
import com.semenov.backend.model.response.MessageResponse;
import com.semenov.backend.service.MedicalRecordService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/medical-records")
@RequiredArgsConstructor
public class MedicalRecordController {
    private final MedicalRecordService medicalRecordService;

    @PostMapping("/create")
    public ResponseEntity<MedicalRecord> createMedicalRecord(@RequestBody MedicalRecord medicalRecord) {
        MedicalRecord createdMedicalRecord = medicalRecordService.createMedicalRecord(medicalRecord);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdMedicalRecord);
    }
    @GetMapping("/{id}")
    public ResponseEntity<MedicalRecord> getMedicalRecordById(@PathVariable Long id) {
        MedicalRecord medicalRecord = medicalRecordService.getMedicalRecordById(id);
        if (medicalRecord != null) {
            return ResponseEntity.ok(medicalRecord);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    @PutMapping("/{id}")
    public ResponseEntity<MessageResponse> updateMedicalRecord(@PathVariable Long id,
                                                               @RequestBody MedicalRecord medicalRecord) {
        medicalRecord.setId(id);
        medicalRecordService.updateMedicalRecord(medicalRecord);
        return ResponseEntity.ok(new MessageResponse("Медицинская карта успешно изменена!",HttpStatus.OK));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<MessageResponse> deleteMedicalRecord(@PathVariable Long id) {
        medicalRecordService.deleteMedicalRecord(id);
        return ResponseEntity.ok(new MessageResponse("Медицинская карта успешно удалена!",HttpStatus.NO_CONTENT));
    }
}
