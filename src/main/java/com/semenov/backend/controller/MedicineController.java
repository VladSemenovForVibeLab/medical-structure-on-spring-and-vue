package com.semenov.backend.controller;

import com.semenov.backend.model.Medicine;
import com.semenov.backend.model.response.MessageResponse;
import com.semenov.backend.service.MedicineService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/medicines")
@RequiredArgsConstructor
public class MedicineController {
    private final MedicineService medicineService;
    @GetMapping("/all")
    public ResponseEntity<List<Medicine>> getAllMedicine() {
        return new ResponseEntity<>(medicineService.getAllMedicines(), HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Optional<Medicine>> getMedicineById(@PathVariable Long id) {
        return new ResponseEntity<>(medicineService.getMedicineById(id),HttpStatus.OK);
    }

    @GetMapping("/search")
    public ResponseEntity<List<Medicine>> searchMedicinesByName(@RequestParam(name = "name") String name) {
        return new ResponseEntity<>(medicineService.getMedicineByName(name),HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Medicine> createMedicine(@RequestBody Medicine medicine) {
        return new ResponseEntity<>(medicineService.createMedicine(medicine),HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Medicine> updateMedicine(@PathVariable Long id, @RequestBody Medicine medicine) {
        return new ResponseEntity<>(medicineService.updateMedicine(id, medicine),HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<MessageResponse> deleteMedicine(@PathVariable Long id) {
        medicineService.deleteMedicine(id);
        return new ResponseEntity<>(new MessageResponse("Лекарство успешно удалено!",HttpStatus.NO_CONTENT),HttpStatus.NO_CONTENT);
    }
}

