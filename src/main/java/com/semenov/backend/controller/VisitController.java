package com.semenov.backend.controller;

import com.semenov.backend.model.Visit;
import com.semenov.backend.model.response.MessageResponse;
import com.semenov.backend.service.VisitService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/visit")
@RequiredArgsConstructor
public class VisitController {
    private final VisitService visitService;
    @PostMapping("/create")
    public ResponseEntity<Visit> createVisit(@RequestBody Visit visit) {
        Visit createdVisit = visitService.createVisit(visit);
        return new ResponseEntity<>(createdVisit, HttpStatus.CREATED);
    }
    @GetMapping("/all")
    public ResponseEntity<List<Visit>> getAllVisits() {
        List<Visit> visits = visitService.getAllVisits();
        return new ResponseEntity<>(visits, HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Visit> getVisitById(@PathVariable Long id) {
        Visit visit = visitService.getVisitById(id);
        return visit != null ? new ResponseEntity<>(visit, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @PutMapping("/{id}")
    public ResponseEntity<Visit> updateVisit(@PathVariable Long id, @RequestBody Visit visit) {
        Visit existingVisit = visitService.getVisitById(id);
        if (existingVisit != null) {
            visit.setId(id);
            Visit updatedVisit = visitService.updateVisit(visit);
            return new ResponseEntity<>(updatedVisit, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<MessageResponse> deleteVisit(@PathVariable Long id) {
        visitService.deleteVisit(id);
        return new ResponseEntity<>(new MessageResponse("Посещение успешно удалено!",HttpStatus.NO_CONTENT),HttpStatus.NO_CONTENT);
    }
}
