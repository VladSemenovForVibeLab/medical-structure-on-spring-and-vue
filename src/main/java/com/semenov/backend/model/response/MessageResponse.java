package com.semenov.backend.model.response;

import lombok.*;
import org.springframework.http.HttpStatus;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class MessageResponse {
    private String message;
    private HttpStatus response;
}
