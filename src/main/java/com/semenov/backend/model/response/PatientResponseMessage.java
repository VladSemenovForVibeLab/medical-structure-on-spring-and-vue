package com.semenov.backend.model.response;

import jakarta.persistence.Column;
import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class PatientResponseMessage {
    private String name;
    private String email;
    private String pNo;
    private String gender;
    private LocalDate dateOfBirth;
    private String address;
    private String phone;
    private String bloodGroup;
    private double weight;
}
