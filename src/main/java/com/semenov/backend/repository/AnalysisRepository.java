package com.semenov.backend.repository;

import com.semenov.backend.model.Analysis;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface AnalysisRepository extends JpaRepository<Analysis, Long> {
    List<Analysis> findByPatientId(Long patientId);

    List<Analysis> findByDateBetween(Date startDate, Date endDate);
}