package com.semenov.backend.repository;

import com.semenov.backend.model.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {
    List<Patient> findByName(String name);

    List<Patient> findByGender(String gender);

    List<Patient> findByBloodGroup(String bloodGroup);
}