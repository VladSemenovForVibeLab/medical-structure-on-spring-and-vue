package com.semenov.backend.service;

import com.semenov.backend.model.Analysis;

import java.util.Date;
import java.util.List;

public interface AnalysisService {
    public Analysis createAnalysis(Analysis analysis);
    public Analysis getAnalysisById(Long id);
    public Analysis updateAnalysis(Analysis analysis);
    public void deleteAnalysis(Long id);
    public List<Analysis> getAnalysesByPatientId(Long patientId);
    public List<Analysis> getAnalysesByDateRange(Date startDate, Date endDate);

}
