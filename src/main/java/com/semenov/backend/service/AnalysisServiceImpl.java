package com.semenov.backend.service;

import com.semenov.backend.exception.NotFoundException;
import com.semenov.backend.model.Analysis;
import com.semenov.backend.repository.AnalysisRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AnalysisServiceImpl implements AnalysisService{
    private final AnalysisRepository analysisRepository;
    @Override
    public Analysis createAnalysis(Analysis analysis) {
        return analysisRepository.save(analysis);
    }

    @Override
    public Analysis getAnalysisById(Long id) {
        return analysisRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Analysis not found with id: " + id));
    }

    @Override
    public Analysis updateAnalysis(Analysis analysis) {
        return analysisRepository.save(analysis);
    }

    @Override
    public void deleteAnalysis(Long id) {
        analysisRepository.deleteById(id);
    }

    @Override
    public List<Analysis> getAnalysesByPatientId(Long patientId) {
        return analysisRepository.findByPatientId(patientId);
    }

    @Override
    public List<Analysis> getAnalysesByDateRange(Date startDate, Date endDate) {
        return analysisRepository.findByDateBetween(startDate, endDate);
    }
}
