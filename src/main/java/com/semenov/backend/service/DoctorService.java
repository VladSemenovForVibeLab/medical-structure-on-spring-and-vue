package com.semenov.backend.service;

import com.semenov.backend.model.Doctor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface DoctorService {
    public Doctor createDoctor(Doctor doctor);
    public List<Doctor> getAllDoctors();
    public Optional<Doctor> getDoctorById(Long id);
    public Doctor updateDoctor(Doctor doctor);
    public void deleteDoctor(Long id);
}
