package com.semenov.backend.service;

import com.semenov.backend.model.MedicalRecord;
import org.springframework.stereotype.Service;

@Service
public interface MedicalRecordService {
    public MedicalRecord createMedicalRecord(MedicalRecord medicalRecord);
    public MedicalRecord getMedicalRecordById(Long id);
    public void updateMedicalRecord(MedicalRecord medicalRecord);
    public void deleteMedicalRecord(Long id);
}
