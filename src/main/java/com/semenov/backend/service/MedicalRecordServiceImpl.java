package com.semenov.backend.service;

import com.semenov.backend.model.MedicalRecord;
import com.semenov.backend.repository.MedicalRecordRepository;
import com.semenov.backend.repository.PatientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MedicalRecordServiceImpl implements MedicalRecordService{
    private final MedicalRecordRepository medicalRecordRepository;
    private final PatientRepository patientRepository;
    @Override
    public MedicalRecord createMedicalRecord(MedicalRecord medicalRecord) {
        patientRepository.save(medicalRecord.getPatient());
        return medicalRecordRepository.save(medicalRecord);
    }

    @Override
    public MedicalRecord getMedicalRecordById(Long id) {
        return medicalRecordRepository.findById(id).orElse(null);
    }

    @Override
    public void updateMedicalRecord(MedicalRecord medicalRecord) {
        if(medicalRecordRepository.existsById(medicalRecord.getId())){
            patientRepository.save(medicalRecord.getPatient());
            medicalRecordRepository.save(medicalRecord);
        }
    }

    @Override
    public void deleteMedicalRecord(Long id) {
        medicalRecordRepository.deleteById(id);
    }
}
