package com.semenov.backend.service;

import com.semenov.backend.model.Medicine;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface MedicineService {
    public List<Medicine> getAllMedicines();
    public Optional<Medicine> getMedicineById(Long id);
    public List<Medicine> getMedicineByName(String name);
    public Medicine createMedicine(Medicine medicine);
    public Medicine updateMedicine(Long id, Medicine medicineDetails);
    public void deleteMedicine(Long id);
}
