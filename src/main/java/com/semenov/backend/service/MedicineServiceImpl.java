package com.semenov.backend.service;

import com.semenov.backend.model.Medicine;
import com.semenov.backend.repository.MedicineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MedicineServiceImpl implements MedicineService {
    private final MedicineRepository medicineRepository;
    @Override
    public List<Medicine> getAllMedicines() {
        return medicineRepository.findAll();
    }

    @Override
    public Optional<Medicine> getMedicineById(Long id) {
        return medicineRepository.findById(id);
    }

    @Override
    public List<Medicine> getMedicineByName(String name) {
        return medicineRepository.findByName(name);
    }

    @Override
    public Medicine createMedicine(Medicine medicine) {
        return medicineRepository.save(medicine);
    }

    @Override
    public Medicine updateMedicine(Long id, Medicine medicineDetails) {
        Optional<Medicine> optionalMedicine = medicineRepository.findById(id);
        if (optionalMedicine.isPresent()) {
            Medicine medicine = optionalMedicine.get();
            medicine.setName(medicineDetails.getName());
            medicine.setManufacturer(medicineDetails.getManufacturer());
            medicine.setActiveIngredient(medicineDetails.getActiveIngredient());
            medicine.setUsageInstructions(medicineDetails.getUsageInstructions());
            medicine.setPrice(medicineDetails.getPrice());
            // Добавьте обновление остальных полей
            return medicineRepository.save(medicine);
        } else {
            throw new RuntimeException("Лекарство с идентификатором " + id + " не найдено");
        }
    }

    @Override
    public void deleteMedicine(Long id) {
        medicineRepository.deleteById(id);
    }
}
