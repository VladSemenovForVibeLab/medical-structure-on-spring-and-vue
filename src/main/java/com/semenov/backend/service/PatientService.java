package com.semenov.backend.service;

import com.semenov.backend.model.Patient;
import com.semenov.backend.model.response.PatientResponseMessage;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PatientService {
    public Patient addPatient(Patient patient);
    public PatientResponseMessage getPatient(Long id);
    public List<Patient> getPatients();
    public void deletePatient(Long id);
    public PatientResponseMessage updatePatient(Long id,Patient patient);

    public List<Patient> searchPatientByName(String name);
    public List<Patient> searchPatientByGender(String gender);

    public List<Patient> searchPatientsByBloodGroup(String bloodGroup);
}
