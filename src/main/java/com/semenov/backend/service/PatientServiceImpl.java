package com.semenov.backend.service;

import com.semenov.backend.model.Patient;
import com.semenov.backend.model.response.PatientResponseMessage;
import com.semenov.backend.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatientServiceImpl implements PatientService{
    @Autowired
    private PatientRepository patientRepository;
    @Override
    public Patient addPatient(Patient patient) {
        return patientRepository.save(patient);
    }

    @Override
    public PatientResponseMessage getPatient(Long id) {
        Patient patient =patientRepository.findById(id).get();
        PatientResponseMessage responseMessage= PatientResponseMessage
                .builder()
                .address(patient.getAddress())
                .bloodGroup(patient.getBloodGroup())
                .dateOfBirth(patient.getDateOfBirth())
                .email(patient.getEmail())
                .gender(patient.getGender())
                .name(patient.getName())
                .phone(patient.getPhone())
                .pNo(patient.getPNo())
                .weight(patient.getWeight())
                .build();
        return responseMessage;
    }

    @Override
    public List<Patient> getPatients() {
        return (List<Patient>) patientRepository.findAll();
    }

    @Override
    public void deletePatient(Long id) {
        patientRepository.deleteById(id);
    }

    @Override
    public PatientResponseMessage updatePatient(Long id, Patient patient) {
        Patient existingPatient = patientRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Пациент с id " + id + " не найден"));

        // Обновление полей
        existingPatient.setAddress(patient.getAddress());
        existingPatient.setName(patient.getName());
        existingPatient.setEmail(patient.getEmail());
        existingPatient.setPNo(patient.getPNo());
        existingPatient.setGender(patient.getGender());
        existingPatient.setDateOfBirth(patient.getDateOfBirth());
        existingPatient.setPhone(patient.getPhone());
        existingPatient.setBloodGroup(patient.getBloodGroup());
        existingPatient.setWeight(patient.getWeight());
        // Сохранение обновленных данных
        patientRepository.save(existingPatient);
        PatientResponseMessage responseMessage = PatientResponseMessage.builder()
                .address(existingPatient.getAddress())
                .bloodGroup(existingPatient.getBloodGroup())
                .dateOfBirth(existingPatient.getDateOfBirth())
                .email(existingPatient.getEmail())
                .gender(existingPatient.getGender())
                .name(existingPatient.getName())
                .phone(existingPatient.getPhone())
                .pNo(existingPatient.getPNo())
                .weight(existingPatient.getWeight())
                .build();

        return responseMessage;
    }

    @Override
    public List<Patient> searchPatientByName(String name) {
        return patientRepository.findByName(name);
    }

    @Override
    public List<Patient> searchPatientByGender(String gender) {
        return patientRepository.findByGender(gender);
    }

    @Override
    public List<Patient> searchPatientsByBloodGroup(String bloodGroup) {
        return patientRepository.findByBloodGroup(bloodGroup);
    }
}
