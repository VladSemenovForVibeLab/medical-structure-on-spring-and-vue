package com.semenov.backend.service;

import com.semenov.backend.model.Visit;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public interface VisitService {
    public Visit createVisit(Visit visit);
    public List<Visit> getAllVisits();
    public Visit getVisitById(Long id);
    public Visit updateVisit(Visit visit);
    public void deleteVisit(Long visitId);
    public List<Visit> getVisitsByPatientId(Long patientId);
}
