package com.semenov.backend.service;

import com.semenov.backend.model.Visit;
import com.semenov.backend.repository.VisitRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class VisitServiceImpl implements VisitService{
    private final VisitRepository visitRepository;
    private final PatientService patientService;
    @Override
    public Visit createVisit(Visit visit) {
        patientService.addPatient(visit.getPatient());
        return visitRepository.save(visit);
    }

    @Override
    public List<Visit> getAllVisits() {
        return visitRepository.findAll();
    }

    @Override
    public Visit getVisitById(Long id) {
        return visitRepository.findById(id).orElse(null);
    }

    @Override
    public Visit updateVisit(Visit visit) {
        patientService.addPatient(visit.getPatient());
        return visitRepository.save(visit);
    }

    @Override
    public void deleteVisit(Long visitId) {
        visitRepository.deleteById(visitId);
    }

    @Override
    public List<Visit> getVisitsByPatientId(Long patientId) {
        return visitRepository.findByPatientId(patientId);
    }
}
